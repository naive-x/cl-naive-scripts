(defsystem "cl-naive-scripts.tests"
  :description "Tests for cl-naive-scripts."
  :version "2024.1.13"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :depends-on (:cl-naive-scripts :cl-naive-tests)
  :components (
               (:file "tests/package")
               (:file "tests/tests" :depends-on ("tests/package"))))

