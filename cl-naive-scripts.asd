(defsystem "cl-naive-scripts"
  :description ""
  :version "2024.1.13"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :depends-on (sandbox closer-mop dissect)
  :components ((:file "src/package")
               (:file "src/common" :depends-on ("src/package"))
               (:file "src/scripts" :depends-on ("src/common"))))

