# -*- mode:org;coding:utf-8 -*-

#+AUTHOR: Phil Marneweck
#+EMAIL: phil@psychedelic.co.za
#+DATE: Sat Jan 13 00:00:00 CEST 2024
#+TITLE: Tutorials for the cl-naive-dom library

#+BEGIN_EXPORT latex
\clearpage
#+END_EXPORT

* Prologue                                                         :noexport:

#+LATEX_HEADER: \usepackage[english]{babel}
#+LATEX_HEADER: \usepackage[autolanguage]{numprint} % Must be loaded *after* babel.
#+LATEX_HEADER: \usepackage{rotating}
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage[margin=0.75in]{geometry}

# LATEX_HEADER: \usepackage{indentfirst}
# LATEX_HEADER: \setlength{\parindent}{0pt}
#+LATEX_HEADER: \usepackage{parskip}

#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{positioning, fit, calc, shapes, arrows}
#+LATEX_HEADER: \usepackage[underline=false]{pgf-umlsd}
#+LATEX_HEADER: \usepackage{lastpage}
#+LATEX_HEADER: \pagestyle{fancyplain}
#+LATEX_HEADER: \pagenumbering{arabic}
#+LATEX_HEADER: \lhead{\small{[Document Name]}}
#+LATEX_HEADER: \lfoot{}
#+LATEX_HEADER: \rfoot{\small{Page \thepage \hspace{1pt} de \pageref{LastPage}}}

* Tutorials

** [Tutorial Name]

[Steps with short descriptions, code and expected output.]

*** [Step Name or Number]

**** Code 

**** Output



# Local Variables:
# eval: (auto-fill-mode 1)
# End:
