;; Enable {} to wrap sexp's in.
(cl-naive-scripts:enable-script-reader-macros)

;; Using two packages with functions and stuff in them to test the
;; scripts with.
(defpackage :not-allowed-test-package
  (:use :cl)
  (:export :test-not-allowed-other))

(in-package :not-allowed-test-package)

(defun test-not-allowed-other ()
  "Not Allowed")

(defpackage :test-package
  (:use :cl)
  (:export :test :test-class :other-slot :another-function :*some-parameter*))

(in-package :test-package)

(defun test ()
  "Test")

(defun test-not-allowed ()
  "Not allowed.")

(defun another-function ()
  "another-function")

(defclass base-class ()
  ((base-slot :initarg :base-slot
              :initform nil
              :accessor base-slot)))

(defclass test-class (base-class)
  ((slot-1 :initarg :slot-1
           :initform nil
           :accessor slot-1)
   (slot-2 :initarg :slot-2
           :initform nil
           :accessor other-slot)))

(defparameter *some-parameter* "Some parameter.")

(in-package :cl-naive-scripts.tests)

(testsuite
    :scripting
  (testcase :include-cl
            :expected "Hello"
            :actual (unwind-protect
                         (multiple-value-bind (result error-report)
                             (run-script nil "(print \"Hello\")")
                           (or error-report result))))

  (testcase :allowed-packages
            :expected "Script execution halted. Not allowed TEST-PACKAGE::TEST-NOT-ALLOWED"
            :actual
            (unwind-protect
                 (multiple-value-bind (result error-report)
                     (run-script
                      nil
                      "(test-package::test-not-allowed)"
                      :script-use-packages '(:test-package)
                      :depends-on-packages '(:test-package))
                   (or error-report result))))

  (testcase :not-allowed-symbol
            :expected "Test"
            :actual
            (unwind-protect
                 (multiple-value-bind (result error-report)
                     (run-script
                      nil
                      "(test)"
                      :script-use-packages '(:test-package)
                      :depends-on-packages '(:test-package))
                   (or error-report result))))

  (testcase :not-allowed-package
            :expected "Can't use symbols from #<PACKAGE \"NOT-ALLOWED-TEST-PACKAGE\">"
            :actual (unwind-protect
                         (multiple-value-bind (result error-report)
                             (run-script
                              nil
                              "(not-allowed-test-package:not-allowed-other)")
                           (if  error-report
                                (subseq error-report 51 111)
                                result))))
  (testcase :read-macro
            :expected "(test)"
            :actual
            (unwind-protect
                 #{(test)}))
  (testcase :cache
            :expected '("script-val" "environment-val")
            :actual (unwind-protect
                         (multiple-value-bind (result error-report)
                             (run-script nil
                                         #{
                                         (setf (get-script-cache "script-val")
                                               "script-val")
                                         (setf (get-environment-cache
                                                "environment-val")
                                               "environment-val")
                                         (list (get-script-cache "script-val")
                                               (get-environment-cache
                                                "environment-val"))
                                         })
                           (or error-report result))))

  (testcase :with-code-run
            :expected "Test"
            :actual
            (unwind-protect
                 (multiple-value-bind (result error-report)
                     (run-script
                      nil
                      #{(test)}
                      :script-use-packages '(:test-package)
                      :depends-on-packages '(:test-package))
                   (or error-report result))))

  (testcase :allowed-package-symbols
            :expected "slot 2"
            :actual (unwind-protect
                         (multiple-value-bind (result error-report)
                             (run-script
                              nil
                              #{
                              (test-package:other-slot
                               (make-instance 'test-package:test-class
                                              :slot-1 "slot 1"
                                              :slot-2 "slot 2"))
                              }

                              :allowed-package-symbols
                              `((test-package
                                 :allow (test-package:test-class
                                         test-package:other-slot))))
                           (or error-report result))))
  (testcase :disallowed-package-symbols
            :expected "Script execution halted. Not allowed TEST-PACKAGE:ANOTHER-FUNCTION"
            :actual (unwind-protect
                         (multiple-value-bind (result error-report)
                             (run-script
                              nil
                              #{
                              (list
                               (test-package:other-slot
                                (make-instance 'test-package:test-class
                                               :slot-1 "slot 1"
                                               :slot-2 "slot 2"))
                               (test-package:another-function))
                              }

                              :allowed-package-symbols
                              `((test-package
                                 :allow (test-package:test-class
                                         test-package:other-slot)
                                 :disallow (test-package:another-function))))
                           (or error-report result))))
  (testcase :test-declaring-func-and-defparameter
            :expected '("xxx" "some parameter changed" "Test" "Local" "Local Parameter")
            :actual (unwind-protect
                         (multiple-value-bind (result error-report)
                             (run-script
                              "temp"
                              #{
                              (defun xxx ()
                                "xxx")
                              (eval-when (:compile-toplevel :load-toplevel :execute)
                                (defparameter *local-parameter* "Local Parameter"))

                              (setf *some-parameter* "some parameter changed")

                              (let ((local "Local"))
                                (list (xxx)
                                      *some-parameter*
                                      (test)
                                      local
                                      *local-parameter*))
                              }
                              :depends-on-packages '(test-package)
                              :script-use-packages '(test-package))
                           (or result error-report))))
  (testcase
   :test-proper-environment-script
   :expected '("xxx" "yyy")
   :actual
   (unwind-protect
        (let* ((environment (make-instance 'environment
                                           :name "test-environment"
                                           :load-once-p t))

               (script-to-run
                 (add-script
                  environment
                  (make-instance
                   'script
                   :transient-package-p t
                   :code #{(list
                            (test-environment.exes::xxx)
                            (test-environment.exes::yyy))
                   }))))

          (add-script
           environment
           (make-instance 'script
                          :preload-script-p t
                          :preload-order 1
                          :name "exes"
                          :export-symbols '(:xxx)
                          :code
                          #{
                          (defun xxx ()
                            "xxx")
                          ;; Can export symbols this way aswell
                          (export-symbol 'yyy)
                          (defun yyy ()
                            "yyy")
                          }))

          (multiple-value-bind (result error-report)
              (run-script environment script-to-run)
            (teardown-environment environment)
            (values result error-report)))))
  (testcase
   :test-preload-error
   :expected "undefined variable: TEST-ENVIRONMENT.EXES::FFF"
   :actual
   (unwind-protect
        (handler-case
            (let* ((*run-debugging* nil)
                   (*debug-load-scripts* t)
                   (environment (make-instance 'environment
                                               :name "test-environment"
                                               :load-once-p t))

                   (script-to-run
                     (add-script
                      environment
                      (make-instance
                       'script
                       :transient-package-p t
                       :code #{(list
                                ;;Test package not allowed error wont
                                ;;happen because the preload will fire
                                ;;an error for the exes scripts.
                                (test-package::test)
                                (test-environment.exes::xxx)
                                (test-environment.exes::yyy))
                       }))))

              (add-script
               environment
               (make-instance 'script
                              :preload-script-p t
                              :preload-order 1
                              :name "exes"
                              :export-symbols '(:xxx)
                              :code
                              #{
                              ;;FFF is undefined and after preloads an
                              ;;error will be raised.
                              fff
                              (defun xxx ()
                                "xxx")
                              ;; Can export symbols this way aswell
                              (export-symbol 'yyy)
                              (defun yyy ()
                                "yyy")
                              }))

              (multiple-value-bind (result error-report)
                  (run-script environment script-to-run)
                (teardown-environment environment)
                (values result error-report)))
          (simple-condition (err)

            (apply 'format nil
                   (simple-condition-format-control err)
                   (simple-condition-format-arguments err)))
          (error (err)
            (error err)))))
  (testcase
   :test-preload-error-reported-in-script-run-values
   :expected t
   :actual
   (unwind-protect
        (let* ((*run-debugging* nil)
               (*debug-load-scripts* nil)
               (environment (make-instance 'environment
                                           :name "test-environment"
                                           :load-once-p t))

               (script-to-run
                 (add-script
                  environment
                  (make-instance
                   'script
                   :transient-package-p t
                   :code #{(list
                            (test-environment.exes::xxx)
                            (test-environment.exes::yyy))
                   }))))

          (add-script
           environment
           (make-instance 'script
                          :preload-script-p t
                          :preload-order 1
                          :name "exes"
                          :export-symbols '(:xxx)
                          :code
                          #{
                          ;; (error "fuck")
                          ;;FFF is undefined and after preloads an
                          ;;preload error will only be reported in
                          ;;the run-script return values.
                          fff
                          (defun xxx ()
                            "xxx")
                          ;; Can export symbols this way aswell
                          (export-symbol 'yyy)
                          (defun yyy ()
                            "yyy")
                          }))

          (multiple-value-bind (result error-report error-environment
                                load-scripts-error-report)
              (run-script environment script-to-run)
            (declare (ignore result error-report  error-environment))

            (teardown-environment environment)
            (when load-scripts-error-report
              t)))))
  (testcase
   :test-error-and-preload-error-reported-in-script-run-values
   :expected t
   :actual
   (unwind-protect
        (let* ((*run-debugging* nil)
               (*debug-load-scripts* nil)
               (environment (make-instance 'environment
                                           :name "test-environment"
                                           :load-once-p t))

               (script-to-run
                 (add-script
                  environment
                  (make-instance
                   'script
                   :transient-package-p t
                   :code #{(list
                            ;;Test package not allowed error will be
                            ;;reported in script-run return values.
                            (test-package::test)
                            (test-environment.exes::xxx)
                            (test-environment.exes::yyy))
                   }))))

          (add-script
           environment
           (make-instance 'script
                          :preload-script-p t
                          :preload-order 1
                          :name "exes"
                          :export-symbols '(:xxx)
                          :code
                          #{
                          ;;FFF is undefined and after preloads an
                          ;;preload error will only be reported in
                          ;;the run-script return values.
                          fff
                          (defun xxx ()
                            "xxx")
                          ;; Can export symbols this way aswell
                          (export-symbol 'yyy)
                          (defun yyy ()
                            "yyy")
                          }))

          (multiple-value-bind (result error-report error-environment load-scripts-error-report)
              (run-script environment script-to-run)
            (declare (ignore result error-environment))

            (teardown-environment environment)
            (when (and error-report load-scripts-error-report)
              t)))))
  (testcase
   :test-symbol-import-from-script
   :expected "xxx"
   :actual
   (unwind-protect
        (let* ((environment (make-instance 'environment
                                           :name "test-environment"
                                           :load-once-p t)))

          (add-script
           environment
           (make-instance 'script
                          :preload-script-p t
                          :preload-order 1
                          :name "exes"
                          :export-symbols '(:xxx)
                          :code
                          #{
                          (defun xxx ()
                            "xxx")
                          }))
          (let ((script-to-run
                  (add-script
                   environment
                   (make-instance
                    'script
                    :transient-package-p t
                    :script-import-symbols `("test-environment.exes:xxx")
                    :code #{
                    (xxx)
                    }))))

            (multiple-value-bind (result error-report)
                (run-script environment script-to-run)
              (teardown-environment environment)
              (values result error-report))))))

  (testcase
   :test-symbol-shadow-from-script
   :expected "xxx"
   :actual
   (unwind-protect
        (let* ((environment (make-instance 'environment
                                           :name "test-environment"
                                           :load-once-p t)))

          (add-script
           environment
           (make-instance 'script
                          :preload-script-p t
                          :preload-order 1
                          :name "exes"
                          :export-symbols '(:xxx)
                          :code
                          #{
                          (defun xxx ()
                            "xxx")
                          }))
          (let ((script-to-run
                  (add-script
                   environment
                   (make-instance
                    'script
                    :transient-package-p t
                    :script-shadow-symbols `("test-environment.exes:xxx")
                    :code #{
                    (intern "XXX")
                    (xxx)
                    }))))

            (multiple-value-bind (result error-report)
                (run-script environment script-to-run)
              (teardown-environment environment)
              (values result error-report))))))

  (testcase
   :test-symbol-import-pass
   :expected "Test"
   :actual
   (unwind-protect
        (let* ((environment (make-instance 'environment
                                           :name "test-environment"
                                           :load-once-p t)))

          (let ((script-to-run
                  (add-script
                   environment
                   (make-instance
                    'script
                    :transient-package-p t
                    :code #{
                    (test)
                    }))))

            (multiple-value-bind (result error-report)
                (run-script environment script-to-run
                            :depends-on-packages '(:test-package)
                            :script-import-symbols `(test-package:test))
              (teardown-environment environment)
              (values result error-report))))))
  (testcase :test-binding
            :expected "lexical?"
            :actual (unwind-protect
                         (let ((test-package:*some-parameter* "lexical?"))
                           (multiple-value-bind (result error-report)
                               (run-script
                                "temp"
                                #{

                                *some-parameter*
                                }
                                :depends-on-packages '(test-package)
                                :script-use-packages '(test-package))
                             (or result error-report)))))
  (testcase :test-warning-muffled
            :expected t
            :actual (unwind-protect
                         (let ((*muffle-warnings* t))
                           (multiple-value-bind (result error-report)
                               (run-script
                                "temp"
                                #{
                                (let ((not-used-warning)))
                                })
                             (when (and (not result) error-report)
                               t)))))
  (testcase :test-warning
            :expected t
            :actual (unwind-protect
                         (let ((*muffle-warnings* nil))
                           (multiple-value-bind (result error-report)
                               (run-script
                                "temp"
                                #{
                                (let ((not-used-warning)))
                                })
                             (when (and (eql result script-failure) error-report)
                               t)))))
  (testcase :cache-symbols
            :expected "hello world"
            :actual (unwind-protect
                         (let* ((environment (make-instance 'environment
                                                            :name "test-environment"
                                                            :load-once-p t)))

                           (let ((script-to-run
                                   (add-script
                                    environment
                                    (make-instance
                                     'script
                                     :transient-package-p t

                                     :code #{
                                     (script-internal:get-script-cache 'hello)
                                     }))))

                             (setf (cl-naive-scripts:get-cache script-to-run 'hello)
                                   "hello world")

                             (multiple-value-bind (result error-report)
                                 (run-script environment script-to-run)
                               (teardown-environment environment)
                               (values result error-report)))))))

;;(cl-naive-tests:run)

