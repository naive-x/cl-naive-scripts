;; Wrapping get-script-cache and get-environment cache in there own
;; package so that they can be exposed to scripts without exposing any
;; other internals to scripts. This way script-internal can be added
;; to the script package use and you don't have to qualify the
;; function names with their packages in scripts.
(defpackage :script-internal
  (:use :cl)
  (:export
   :get-script-cache
   :get-environment-cache
   :export-symbol
   :import-symbol))

(in-package :script-internal)

(defun get-script-cache (accessor)
  (cl-naive-scripts:get-cache cl-naive-scripts:*script* accessor))

(defun (setf get-script-cache) (value accessor)
  (setf (cl-naive-scripts:get-cache cl-naive-scripts:*script* accessor) value))

(defun get-environment-cache (accessor)
  (cl-naive-scripts:get-cache cl-naive-scripts:*environment* accessor))

(defun (setf get-environment-cache) (value accessor)
  (setf (cl-naive-scripts:get-cache cl-naive-scripts:*environment* accessor) value))

(defun export-symbol (symbol)
  (eval-when (:execute)
    (export (find-symbol (format nil "~A" symbol) *package*)
            *package*)))

(in-package :cl-naive-scripts)

(defclass script ()
  ((name
    :initarg :name
    :accessor name
    :initform nil
    :documentation "Name of the script. Needs to be something that can be interpreted/converted as a sane symbol.")
   (script-package
    :initarg :script-package
    :accessor script-package
    :initform nil
    :documentation "The package to use with the script.

Can be explicitly set or a package will be created for the script
named like this [environment name].[script name].

If transient-pacakge-p is t the system will delete the package created
after the run.

If you are explicitly going to supply a package you need to make sure
the that the package name is sane to use and obviously take care of
any package name clashes.

Setting the package explicitly has the following consequences:

1. script-use-packages will be ignored.  export-symbols will be ignored.  The
2. Package symbols will not be added to the environment allowed
symbols automatically so that other scripts can use it. You will have
to manually add any symbols from the package to the allowed symbols.

Warning: Don't set transient-package-p to t if you are going to explicitly
set the package because temporary packages are deleted by the system
after a run. The system tries to do the best thing by only deleting
packages with names in the format of [environment name].[script name]
but nothing is guaranteed.

")
   (script-use-packages
    :initarg :script-use-packages
    :accessor script-use-packages
    :initform nil
    :documentation "A list of packges to use in the script.")
   (script-import-symbols
    :initarg :script-import-symbols
    :accessor script-import-symbols
    :initform nil
    :documentation "A list of symbols and/or symbol names the script needs to import.

When you are trying to import symbols from other scripts in the same
run you need to use the symbol name as a string.")
   (script-shadow-symbols
    :initarg :script-shadow-symbols
    :accessor script-shadow-symbols
    :initform nil
    :documentation "A list of symbols and/or symbol names the script needs to shadow.

When you are trying to shadow symbols from other scripts in the same
run you need to use the symbol name as a string.

NOTE: You have to be carefull here because if you have a defun or
something that creates the symbol in the code of the script before you
use that symbol it will redefine it. So you can effect only shadow
symbols imported by use.")
   (depends-on-packages
    :initarg :depends-on-packages
    :accessor depends-on-packages
    :initform nil
    :documentation "A list of packages the script depends on.

Symbols that are external to the packages will be add to the allowed
symbols list automatically.")
   (export-symbols
    :initarg :export-symbols
    :accessor export-symbols
    :initform nil
    :documentation "A list of symbols to export for a  dynamically created script package.

This is ignored if the script-package is set explicitly.")
   (allowed-package-symbols
    :initarg :allowed-package-symbols
    :accessor allowed-package-symbols
    :initform nil
    :documentation "Packages symbols allowed for the script.

If not explicitly set depends-on-packages will be used to calculate
the allowed symbols the first time the script is used. The calculated
list will be put in this slot.

Warning: If allowed-package-symbols is explicitly set there will be no attempt
to derive allowed-symbols.

The value set must be of the following form.

(([package]
  :allow ([list of [pacakge]:[symbol]])
  :disallow ([list of [pacakge]:[symbol]])))

For example:

`((,(find-package \"MY-PACKAGE\")
    (my-package:some-function
     my-package:some-other-function)))
")
   (code
    :initarg :code
    :accessor code
    :initform nil
    :documentation "The code of the script.

Code can be a string or a sexp.

Warning: There are issues when using a sexp. You need to be cognisant
of when the sexp will be read by CL. Reading interns symbols and you
might find yourself fighting stupid not allowed symbol bugs. It is
advised that you rather wrap the sexp in the special code reader macro
#{...}. This will make sure that the code is only read just before the
evaluation of the script with the right context/environment setup.")
   (compiled-code
    :initarg :compiled-code
    :accessor compiled-code
    :initform nil
    :documentation "The compiled code of the script.

Code is only compiled when *compile-scripts* is t.  Code is compiled
the first time that a script is run and never recompiled for the life
of the script object unless you force-recompile during a run.")

   (cache
    :initarg :cache
    :accessor cache
    :initform (make-hash-table :test 'equalp)
    :documentation "Cache hash-table to store values associated with the script. The cache is cleared after each run so if you want to store values for later use, use the environment cache.

You dont have to use the hashtable directly there are convenience
functions to add and remove values from the cache.

Values from the cache can be retrieved and setf using get-script-cache
in script code itself.
")
   (preload-script-p
    :initarg :preload-script-p
    :accessor preload-script-p
    :initform nil
    :documentation "Indicates if a script should be loaded on environment initialisation.

Scripts that are marked thus are loaded in order when the environment is
initialised. The environment is automatically initialised the first
time it is used in run-script.

You can also explicitly initialise an environment before using
run-script for the first time.
")
   (preload-order
    :initarg :preload-order
    :accessor preload-order
    :initform 999
    :documentation "This is used to sort scripts that are preloaded into the correct order to load, on environment initialisation.")
   (transient-package-p
    :initarg :transient-package-p
    :accessor transient-package-p
    :initform nil
    :documentation "If a dynamically created package exists it will be deleted at the end of the run.

Also won't set the script-package slot during a run if it was not
explicitly set as is done for scripts that are pre-loaded usually."))
  (:documentation "A class that represents a script and all the attributes needed to initialise run the script."))

(defgeneric get-cache (object accessor &key &allow-other-keys)
  (:documentation "Used to get and set cache values for an environment or a script."))

(defmethod get-cache ((script script) accessor &key &allow-other-keys)
  ""
  (gethash (format nil "~A"  accessor) (cache script)))

(defmethod (setf get-cache) (value (script script) accessor
                             &key remove-entry-p &allow-other-keys)
  ""
  (if (and (not value) remove-entry-p)
      (remhash (format nil "~A"  accessor) (cache script))
      (setf (gethash (format nil "~A"  accessor) (cache script)) value)))

(defclass environment ()
  ((name
    :initarg :name
    :accessor name
    :initform nil
    :documentation "Name of the environment. Needs to be something that can be
interpreted/converted as a sane symbol.")
   (allowed-package-symbols
    :initarg :allowed-package-symbols
    :accessor allowed-package-symbols
    :initform nil
    :documentation "Package symbol list allowed when running script.

This can be set directly initially, but scripts will add to this list
as they are used. So be carefull to not override what scripts added
over time.
")
   (allowed-package-symbols%
    :initarg :allowed-package-symbols%
    :accessor allowed-package-symbols%
    :initform nil
    :documentation "Package symbol list allowed when running temporary scripts or scripts
with temporary packages.

This should never be set directly by a user.
")
   (scripts
    :initarg :scripts
    :accessor scripts
    :initform (make-hash-table :test 'equalp)
    :documentation "Scripts available in the environment.")
   (cache
    :initarg :cache
    :accessor cache
    :initform (make-hash-table :test 'equalp)
    :documentation "Cache hash-table to store values associated with environment.

You dont have to use the hashtable directly there are convenience
functions to add and remove values from the cache.

Values from the cache can be retrieved and setf using get-environment-cache
in script code itself.
")
   (load-once-p
    :initarg :load-once-p
    :accessor load-once-p
    :initform t
    :documentation "If set the load scripts will only be loaded the first time run-script is called or setup-environment is called manually.

It is defaulted to t because reloading scripts on every run is expensive and should only be done in specific circumstances.")
   (scripts-loaded-p
    :initarg :scripts-loaded-p
    :accessor scripts-loaded-p
    :initform nil
    :documentation "If the scripts are loaded it will be t.

You should not set this slot value yourself it is for information only.")
   (initialized-p
    :initarg :initialized-p
    :accessor initialized-p
    :initform nil
    :documentation "Indicates if an environment has already been initialized.

You should not set this explicitly.")
   (run-package
    :initarg :run-package
    :accessor run-package
    :initform nil
    :documentation "Package used to run the enviroment scripts. Used during runs and set
during run initialization."))
  (:documentation "A class that represents a script environment.

An environment contains one or more scripts."))

(defun get-environment (name)
  "Used to retrieve an environment from *environments*."
  (gethash name *environments*))

(defgeneric add-environment (environment)
  (:documentation "Adds an environment to *environments* using the name as key."))

(defmethod add-environment ((environment environment))
  (setf (gethash (name environment) *environments*) environment))

(defgeneric teardown-environment (environment)
  (:documentation "Specialise to do custom tear down of the environment.

There is an around method that removes any packages created by the
environment already so you don't have to take care of that. Obviously
if you dont want packages to be deleted you will have to specialize
the around as well."))

(defmethod teardown-environment :around ((environment environment))
  (maphash (lambda (name script)
             (declare (ignore name))
             ;; To try and not remove packages that was not created on the fly
             ;; we check the name. Of course if an explicitly created package
             ;; uses the same naming convention things are going to go wrong.

             ;; TODO: Should we somehow mark if the package was created on the
             ;; fly or not to prevent accidents?
             (when (or
                    (transient-package-p script)
                    (and (script-package script)
                         (equalp (package-name (script-package script))
                                 (format nil "~A.~A"
                                         (name environment)
                                         (name script)))))
               (ignore-errors
                (when (script-package script)
                  (delete-package (script-package script))))))
           (scripts environment)))

(defmethod teardown-environment ((environment environment))
  nil)

(defgeneric remove-environment (environment)
  (:documentation "Removes an environment from *environments*.

teardown-environment is called for the environment as well."))

(defmethod remove-environment ((environment environment))
  (remhash (name environment) *environments*)
  (teardown-environment environment))

(defmethod remove-environment ((environment string))
  (remhash environment *environments*)
  (teardown-environment (get-environment environment)))

(defmethod remove-environment ((environment symbol))
  (remhash environment *environments*)
  (teardown-environment (get-environment environment)))

(defmethod get-cache ((environment environment) accessor &key &allow-other-keys)
  ""
  (gethash (format nil "~A"  accessor) (cache environment)))

(defmethod (setf get-cache) (value (environment environment) accessor
                             &key remove-entry-p &allow-other-keys)
  "Sets a cache value. If the value is nil and remove-entry-p is t then the entry will be removed completely from the cache."
  (if (and (not value) remove-entry-p)
      (remhash (format nil "~A"  accessor) (cache environment))
      (setf (gethash (format nil "~A"  accessor) (cache environment)) value)))

(defgeneric get-script (environment name)
  (:documentation "Retrieves a script form the environment by name."))

(defmethod get-script ((environment environment) name)
  (gethash name (scripts environment)))

(defgeneric add-script (environment script)
  (:documentation "Adds a script to the environment."))

(defmethod add-script ((environment environment) (script script))
  (setf (gethash (name script) (scripts environment)) script))

;; TODO: It is nearly impossible to remove a script cleanly because
;; the way allowed-package-symbols is stored in the environment. Will
;; have to store it with the script name to make it possible but doing
;; that will make it a lot more inconvenient for the user to
;; explicitly specifying allowed-package-symbols. Can revisit this
;; once we have seen some use.

(defgeneric remove-script (code environment))

(defmethod remove-script ((environment environment) (script script))
  (remhash (name script) (scripts environment)))

(defgeneric load-scripts (environment)
  (:documentation "Takes all scripts marked for preloading and runs them in the
preloading order specified.

Warning: If you call this function explicitly scripts will be loaded
regardless of the preload-scripts-p setting of the environment. It
will also load all scripts again regardless of the fact that they
could have been already loaded.

This function is called by initialize-environment if the environment
is set to preload scripts in preload-scripts-p and it won't try to
load scripts again if load-once-p is set for the environment."))

(defmethod load-scripts ((environment environment))
  (let ((scripts))
    (maphash (lambda (name script)
               (declare (ignore name))
               (when (preload-script-p script)
                 (push script scripts)))
             (scripts environment))

    (dolist (script (sort scripts '< :key 'preload-order))
      (when (preload-script-p script)
        (multiple-value-bind (result message stack)
            (run-script environment script)
          (declare (ignore result))

          (when message
            (push (list :script-name (name script)
                        :message message
                        :stack stack)
                  *preload-script-errors*)))

        (unless (transient-package-p script)

          ;;Trying to only expose packages that where automatically
          ;;created for the script. If a package was supplied then it
          ;;is up to the user to add the package to allowed manually if needed.
          (when (equalp (package-name (script-package script))
                        (format nil "~A.~A" (name environment) (name script)))
            ;; Have to export the symbols of the script package first so
            ;; they can be added to allowed for use by other scripts.
            (dolist (symbol (export-symbols script))
              (export (find-symbol (format nil "~A" symbol) (script-package script))
                      (script-package script)))

            ;; Adding load scripts to the allowed list for use by other
            ;; scripts in the environment
            (setf (allowed-package-symbols environment)
                  (merge-allowed-package-symbols
                   (allowed-package-symbols environment)
                   (devine-allowed-symbols (list (script-package script))
                                           :only-external-symbols-p t)))))))

    (when (and *debug-load-scripts* *preload-script-errors*)
      (error "There where errors during the loading of preload scripts:~%~%~S"
             (reverse *preload-script-errors*)))))

(defgeneric initialize-environment (environment &key preload-scripts-p)
  (:documentation "Used to initialise an environment.

There is an :around method to load any scripts that need to be preloaded.

This is called before each run-script but wont do anything if the
enviroment has been initialized already."))

(defmethod initialize-environment :around ((environment environment)
                                           &key preload-scripts-p)
  (if (not (initialized-p environment))
      (progn

        (when (or (not (load-once-p environment))
                  (and preload-scripts-p (not (scripts-loaded-p environment))))
          ;; Setting initialized-p before load-scripts to prevent init
          ;; from going into an endless loop
          (setf (initialized-p environment) t)
          (load-scripts environment))
        (prog1
            (call-next-method)
          (setf (initialized-p environment) t)))
      (call-next-method)))

(defmethod initialize-environment ((environment environment)
                                   &key preload-scripts-p)
  "Does nothing just there to make sure there is an existing method."
  (declare (ignore environment preload-scripts-p))
  nil)

(defgeneric initialize-run (environment script
                            &key package depends-on-packages script-use-packages
                            allowed-package-symbols
                            script-import-symbols
                            script-shadow-symbols)
  (:documentation "A lot of work is done here to ensure the success of a run.

If the script does not have a tansient package and allowed-package
symbols where not set explicitly then the system tries to devine
allowed symbols. See code for more details.

Any devined allowed symbols are added to the environment's
allowed-package-symbols.

If the script does not have a package one is created for it."))

(defun allowed-stuff (list)
  (let ((stuff (make-hash-table :test 'equalp)))
    (dolist (package list)
      (let ((package-name (package-name (first package))))
        (dolist (symbol (second package))
          (pushnew symbol (gethash package-name stuff)))))
    stuff))

(defun devine-allowed-symbols-for-script (script &key depends-on-packages)
  ;;TODO: Fixed this so that code aligns with documentation of
  ;;allowed-package-symbols for scripts. IE dont try to devine if
  ;;explicitly set. But should we not rather merge the sets?
  (unless (allowed-package-symbols script)
    (let ((allowed-stuff (make-hash-table :test 'equalp))
          (allowed-symbols))

      (dolist (package (append depends-on-packages (depends-on-packages script)))
        (dolist (symbol (package-symbols package))
          (pushnew symbol (gethash package allowed-stuff))))

      (maphash (lambda (key allowed)
                 (setf allowed-symbols
                       (append allowed-symbols
                               (list
                                (list (find-package key)
                                      :allow allowed)))))
               allowed-stuff)

      (setf (allowed-package-symbols script) allowed-symbols))))

(defun import-symbol (environment symbol import-package &key shadow-p)
  (let* ((symbolx (if (stringp symbol)
                      (let ((symbol-split
                              (uiop:split-string symbol :separator '(#\:))))
                        (if (> (length symbol-split) 1)
                            (let ((symbol-package
                                    (find-package (string-upcase (car symbol-split)))))
                              (find-symbol (string-upcase
                                            (car (last symbol-split)))
                                           symbol-package))
                            (find-symbol (string-upcase (car symbol-split)))))
                      symbol))
         (package
           (symbol-package symbolx))
         (allowed-p (or
                     (loop for packagex in (allowed-package-symbols environment)
                           when (equal package (car packagex))
                           return (find symbolx (getf (cdr packagex) :allow)))
                     (loop for packagex in (allowed-package-symbols% environment)
                           when (equal package (car packagex))
                           return (find symbolx (getf (cdr packagex) :allow)))
                     (loop for packagex in sandbox::*allowed-packages*
                           when (equal package (car packagex))
                           return (find symbolx (getf (cdr packagex) :allow))))))

    (if allowed-p
        (if shadow-p
            (shadowing-import symbolx import-package)
            (import symbolx import-package))

        (error "Symbol ~S not allowed." symbolx))))

(defmethod initialize-run :around ((environment environment) (script script)
                                   &key package depends-on-packages script-use-packages
                                   allowed-package-symbols
                                   script-import-symbols
                                   script-shadow-symbols)

  (if (and (not (allowed-package-symbols script))
           (not (transient-package-p script)))
      (let ((allowed-symbols (devine-allowed-symbols-for-script
                              script
                              :depends-on-packages depends-on-packages)))

        (setf (allowed-package-symbols environment) nil)
        (setf (allowed-package-symbols script)
              (merge-allowed-package-symbols (allowed-package-symbols script)
                                             allowed-package-symbols
                                             allowed-symbols))
        (setf (allowed-package-symbols environment)
              (merge-allowed-package-symbols (allowed-package-symbols environment)
                                             (allowed-package-symbols script))))

      (if (transient-package-p script)
          (let ((allowed-symbols (devine-allowed-symbols-for-script
                                  script
                                  :depends-on-packages depends-on-packages)))

            (setf (allowed-package-symbols script)
                  (merge-allowed-package-symbols (allowed-package-symbols script)
                                                 allowed-package-symbols
                                                 allowed-symbols))

            (setf (allowed-package-symbols% environment)
                  (merge-allowed-package-symbols
                   (allowed-package-symbols% environment)
                   (allowed-package-symbols script))))
          ;; TODO: We don't know if this was done before, we could set a
          ;; private slot on the script to prevent redoing this work but
          ;; then there would be no opportunity to amend the list on the
          ;; fly without adding functions to deal with amendmends that can
          ;; deal with the flag correctly.
          (setf (allowed-package-symbols environment)
                (merge-allowed-package-symbols (allowed-package-symbols environment)
                                               (allowed-package-symbols script)))))

  (setf (run-package environment)
        (or
         package
         (script-package script)
         ;; Scripts that are marked as load scripts, should have
         ;; stable/persistant packages if you want to be able to stuff
         ;; from one script in another.

         ;; To prevent package name clashes between environments we
         ;; prepend the environment name to the script name like this
         ;; [environment name].[script name] to create a package for
         ;; the script.
         (let ((package (or
                         (find-package
                          (format nil "~A.~A"
                                  (string-upcase (name environment))
                                  (string-upcase (name script))))
                         (make-package
                          (format nil "~A.~A"
                                  (string-upcase (name environment))
                                  (string-upcase (name script)))
                          :use (let ((use (or script-use-packages (script-use-packages script))))
                                 (pushnew :cl use)
                                 (pushnew :script-internal use)
                                 use)))))

           ;; (break "??? ~S~%~%~S" package (list-all-packages))

           (when (and (preload-script-p script) (not (transient-package-p script)))
             ;;(break "~S~%~%~S" package (list-all-packages))
             (setf (script-package script) package))
           package)))

  ;; At this stage the script package exists and in theory any
  ;; preloaded script packages so we should be able to import any
  ;; symbols successfully here.

  (dolist (symbol (append script-import-symbols (script-import-symbols script)))
    (import-symbol environment symbol (run-package environment)))

  (dolist (symbol (append script-shadow-symbols (script-shadow-symbols script)))
    (import-symbol environment symbol (run-package environment) :shadow-p t))

  (call-next-method))

(defmethod initialize-run ((environment environment) (script script)
                           &key package depends-on-packages script-use-packages
                           allowed-package-symbols
                           script-import-symbols
                           script-shadow-symbols)
  "Does nothing just there to make sure there is an existing method."
  (declare (ignore environment script package depends-on-packages script-use-packages
                   allowed-package-symbols script-import-symbols
                   script-shadow-symbols))
  nil)

(defgeneric teardown-run (environment script)
  (:documentation "Called to clean up after a run.

This is called after a run, regardless if the run was successful."))

(defmethod teardown-run :around ((environment environment) (script script))
  "The script cahce is cleared.

And any transient packages are deleted."
  (setf (cache script) (make-hash-table :test 'equalp))

  (if (or
       (and (script-package script)
            (not (preload-script-p script))
            (equalp (package-name (script-package script))
                    (format nil "~A.~A" (name environment) (name script)))))
      (delete-package (script-package script))
      (when (or (transient-package-p script)
                (not (script-package script)))
        ;;TODO: see why we end up with run-package nil at some
        ;;times!!! Should not happend. Possibly running one script in
        ;;another.
        (when (run-package environment)
          (delete-package (run-package environment)))))

  (setf (run-package environment) nil)

  (call-next-method))

(defmethod teardown-run ((environment environment) (script script))
  (declare (ignore environment script))
  "Does nothing just there to make sure there is an existing method."
  nil)

(defun compile-script% (script)
  ""
  (if (not *run-debugging*)
      ;;NOTE: Previously we use handler case but that introduces
      ;;issues with defparameter variables being declared
      ;;undefined. If you understand why please inform me.
      (let ((warnings nil))
        (handler-bind
            ((error #'(lambda (err)
                        (etypecase err
                          (sandbox:not-allowed
                           (let ((message
                                   (format nil "Script execution halted. Not allowed ~S"
                                           (slot-value
                                            err
                                            'sandbox::name))))
                             (return-from compile-script%
                               (values script-failure
                                       message
                                       (dissect:capture-environment err)
                                       (reverse *preload-script-errors*)))))
                          (simple-error
                           (let ((message
                                   (concatenate
                                    'string
                                    (format
                                     nil
                                     "Script execution halted with the following error:~%~%~A~%~%"

                                     (apply 'format nil
                                            (simple-condition-format-control err)
                                            (simple-condition-format-arguments err)))
                                    (dissect:present
                                     (strip-stack (dissect:stack))
                                     nil))))

                             (return-from compile-script%
                               (values script-failure
                                       message
                                       (dissect:capture-environment err)
                                       (reverse *preload-script-errors*)))))

                          (t
                           (return-from compile-script%
                             (values
                              script-failure
                              (concatenate
                               'string
                               (format nil
                                       "Script execution halted with the following error:~%~%~A~%~%"
                                       err)
                               (dissect:present
                                (strip-stack (dissect:stack))
                                nil))
                              (dissect:capture-environment err)
                              *preload-script-errors*))))))
             (warning #'(lambda (err)
                          (if *muffle-warnings*
                              (progn
                                (setf warnings (list
                                                (concatenate
                                                 'string
                                                 (format
                                                  nil
                                                  "The following warning was raised during script execution:~%~%~A~%~%"
                                                  (apply 'format nil
                                                         (simple-condition-format-control err)
                                                         (simple-condition-format-arguments err)))
                                                 (dissect:present
                                                  (strip-stack (dissect:stack))
                                                  nil))
                                                (dissect:capture-environment err)
                                                (reverse *preload-script-errors*)))
                                (continue))

                              (return-from compile-script%
                                (list
                                 (concatenate
                                  'string
                                  (format
                                   nil
                                   "Script execution halted with the following warning:~%~%~A~%~%"
                                   (apply 'format nil
                                          (simple-condition-format-control err)
                                          (simple-condition-format-arguments err)))
                                  (dissect:present
                                   (strip-stack (dissect:stack))
                                   nil))
                                 (dissect:capture-environment err)
                                 (reverse *preload-script-errors*)))))))

          (let ((sandbox::*allowed-packages*
                  (append sandbox::*allowed-packages*
                          (list (list *package*)))))
            (values
             (sandbox:sandbox-compile
              (if (stringp script)
                  (sandbox:read-from-string
                   (format nil "(block nil ~A)" script))
                  (if (and (listp script) (consp (car script)))
                      `(block nil ,@script)
                      `(block nil ,script))))
             (first warnings)
             (second warnings)
             (third warnings))))

        (handler-bind ((error #'invoke-debugger))
          (let ((sandbox::*allowed-packages*
                  (append sandbox::*allowed-packages*
                          (list (list *package*)))))

            (values
             (sandbox:sandbox-compile
              (if (stringp script)
                  (sandbox:read-from-string
                   (format nil "(block nil ~A)" script))
                  (if (and (listp script) (consp (car script)))
                      `(block nil ,@script)
                      `(block nil ,script))))
             nil
             nil))))))

(defun run-script% (script)
  ""
  (if (not *run-debugging*)
      (let ((warnings nil))
        (handler-bind
            ((error #'(lambda (err)
                        (etypecase err
                          (sandbox:not-allowed
                           (let ((message
                                   (format nil "Script execution halted. Not allowed ~S"
                                           (slot-value
                                            err
                                            'sandbox::name))))
                             (return-from run-script%
                               (values script-failure
                                       message
                                       (dissect:capture-environment err)
                                       (reverse *preload-script-errors*)))))
                          (simple-error
                           (let ((message
                                   (concatenate
                                    'string
                                    (format
                                     nil
                                     "Script execution halted with the following error:~%~%~A~%~%"
                                     (apply 'format nil
                                            (simple-condition-format-control err)
                                            (simple-condition-format-arguments err)))
                                    (dissect:present
                                     (strip-stack (dissect:stack))
                                     nil))))

                             (return-from run-script%
                               (values script-failure
                                       message
                                       (dissect:capture-environment err)
                                       (reverse *preload-script-errors*)))))

                          (t
                           (return-from run-script%
                             (values
                              script-failure
                              (concatenate
                               'string
                               (format nil
                                       "Script execution halted with the following error:~%~%~A~%~%"
                                       err)
                               (dissect:present
                                (strip-stack (dissect:stack))
                                nil))
                              (dissect:capture-environment err)
                              *preload-script-errors*))))))
             (warning #'(lambda (err)
                          (if *muffle-warnings*
                              (progn
                                (setf warnings
                                      (list
                                       (concatenate
                                        'string
                                        (format
                                         nil
                                         "The following warning was raised during script execution:~%~%~A~%~%"
                                         (if (slot-exists-p
                                              err
                                              'simple-condition-format-control)
                                             (apply 'format nil
                                                    (simple-condition-format-control err)
                                                    (simple-condition-format-arguments
                                                     err)))
                                         (format nil "~S" err))
                                        (dissect:present
                                         (strip-stack (dissect:stack))
                                         nil))
                                       (dissect:capture-environment err)
                                       (reverse *preload-script-errors*)))
                                (continue))

                              (return-from run-script%
                                (values
                                 script-failure
                                 (concatenate
                                  'string
                                  (format
                                   nil
                                   "Script execution halted with the following warning:~%~%~A~%~%"
                                   (apply 'format nil
                                          (simple-condition-format-control err)
                                          (simple-condition-format-arguments err)))
                                  (dissect:present
                                   (strip-stack (dissect:stack))
                                   nil))
                                 (dissect:capture-environment err)
                                 (reverse *preload-script-errors*)))))))

          (let ((sandbox::*allowed-packages*
                  (append sandbox::*allowed-packages*
                          (list (list *package*)))))
            (values
             (sandbox:eval
              (if (stringp script)
                  (sandbox:read-from-string
                   (format nil "(block nil ~A)" script))
                  (if (and (listp script) (consp (car script)))
                      `(block nil ,@script)
                      `(block nil ,script))))
             (first warnings)
             (second warnings)
             (third warnings)))))
      (handler-bind ((error #'invoke-debugger))
        (let ((sandbox::*allowed-packages*
                (append sandbox::*allowed-packages*
                        (list (list *package*)))))

          (values
           (sandbox:eval
            (if (stringp script)
                (sandbox:read-from-string
                 (format nil "(block nil ~A)" script))
                (if (and (listp script) (consp (car script)))
                    `(block nil ,@script)
                    `(block nil ,script))))
           nil
           nil)))))

(defgeneric run-script (environment script &key package depends-on-packages
                        script-use-packages
                        allowed-package-symbols
                        script-import-symbols
                        script-shadow-symbols
                        force-recompile-p)
  (:documentation "Runs the script using the supplied environment and script.

The script is wrapped in a (block nil ...) so you can use (return)
or (return-from nil [return value]) to exit the script at any time.

Uses sandbox:eval to run the script.

Returns multiple values:

1. Script Result: What ever the script returns. Will be set to :script-failure if the script compile or execution has an warning (if warnings is not muffled) or error.

2. Error/Warning Report: A shortened error report.

3. Error/Warning Environment: A copy of the condition, stack and some other info.

4. Load Scripts Error/Warning Report: A list of errors that occured during the
loading of preload-scripts.
"))

(defmethod run-script :around ((environment environment) (script script)
                               &key package depends-on-packages script-use-packages
                               allowed-package-symbols
                               script-import-symbols
                               script-shadow-symbols
                               force-recompile-p)
  "Initialises the environment first if not already initialised."
  (declare (ignorable script package depends-on-packages script-use-packages
                      allowed-package-symbols script-import-symbols
                      script-shadow-symbols
                      force-recompile-p))

  (let ((*preload-script-errors* *preload-script-errors*))
    (unless (initialized-p environment)
      (initialize-environment environment :preload-scripts-p t))
    (multiple-value-bind (result message condition-environment)
        (call-next-method)
      (values result message condition-environment *preload-script-errors*))))

(defmethod run-script (environment code &key package depends-on-packages
                       script-use-packages
                       allowed-package-symbols
                       script-import-symbols
                       script-shadow-symbols
                       force-recompile-p)
  "If the environment is nil an environment is created on the fly.

If the environment is the name of the environment then it will use
get-environment to try and retrieve the environment. If the
envitonment does not exist it will create one on the fly.

A script object is created with the passed code and other parameters
passed that are relevant."
  (let* ((script (make-instance
                  'script
                  :code code
                  :transient-package-p t
                  :script-use-packages script-use-packages
                  :allowed-package-symbols allowed-package-symbols
                  :script-package package
                  :name (format nil "~A" (gensym))))
         (environment (or (get-environment environment)
                          (make-instance
                           'environment
                           ;; :allowed-package-symbols allowed-package-symbols
                           :name (or (format nil "~A" (gensym)) environment)))))

    (add-script environment script)

    (multiple-value-bind (result message condition-environment)
        (run-script environment script
                    :package package
                    :depends-on-packages depends-on-packages
                    :script-use-packages script-use-packages
                    :allowed-package-symbols allowed-package-symbols
                    :script-import-symbols script-import-symbols
                    :script-shadow-symbols script-shadow-symbols
                    :force-recompile-p force-recompile-p)
      (values result message condition-environment))))

(defmethod run-script ((environment environment) (code string)
                       &key package depends-on-packages script-use-packages
                       allowed-package-symbols
                       script-import-symbols
                       script-shadow-symbols
                       force-recompile-p)
  "A script object is created with the passed code and other parameters
passed that are relevant."

  (let* ((script (make-instance
                  'script
                  :code code
                  :transient-package-p t
                  :allowed-package-symbols allowed-package-symbols
                  :script-package package
                  :name (format nil "~A" (gensym)))))

    (add-script environment script)

    (multiple-value-bind (result message condition-environment)
        (run-script environment script
                    :package package
                    :depends-on-packages depends-on-packages
                    :script-use-packages script-use-packages
                    :allowed-package-symbols allowed-package-symbols
                    :script-import-symbols script-import-symbols
                    :script-shadow-symbols script-shadow-symbols
                    :force-recompile-p force-recompile-p)

      (remove-script environment script)
      (values result message condition-environment))))

(defmethod run-script ((environment environment) (script script)
                       &key package depends-on-packages script-use-packages
                       allowed-package-symbols
                       script-import-symbols
                       script-shadow-symbols
                       force-recompile-p)

  (initialize-run environment script
                  :package package
                  :depends-on-packages depends-on-packages
                  :script-use-packages script-use-packages
                  :allowed-package-symbols allowed-package-symbols
                  :script-import-symbols script-import-symbols
                  :script-shadow-symbols script-shadow-symbols)

  (let ((*package* (run-package environment))
        (*environment* environment)
        (*script* script)
        (sandbox::*allowed-packages*
          (append
           sandbox::*allowed-packages*
           (list (list (find-package "SCRIPT-INTERNAL")
                       :allow '(script-internal:get-script-cache
                                script-internal:get-environment-cache
                                script-internal:export-symbol)))
           (allowed-package-symbols environment)
           (allowed-package-symbols% environment))))

    (multiple-value-bind (result message condition-environment)
        (run-script% (or (or (and (not force-recompile-p) (compiled-code script))
                             (and *compile-scripts*
                                  (setf (compiled-code script)
                                        (compile-script% (code script)))))
                         (code script)))
      (teardown-run environment script)
      (values result message condition-environment))))

