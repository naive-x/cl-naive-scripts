(in-package :cl-naive-scripts)

(defvar *script*
  "This is set to the running script for use by cache functions.
A user should not be setting this.")

(defvar *environment*
  "This is set to the environment used by the run, for use by cache functions.
A user should not be setting this.")

(defgeneric package-symbols (package &key only-external-symbols-p)
  (:documentation "Compiles a list of symbols for the package.

only-external-symbols-p will only include external symbols for the
package. This is the default."))

(defmethod package-symbols ((package symbol) &key (only-external-symbols-p t))
  (package-symbols
   (find-package package)
   :only-external-symbols-p
   only-external-symbols-p))

(defmethod package-symbols ((package package) &key (only-external-symbols-p t))
  (let ((symbols))
    (if only-external-symbols-p
        (do-external-symbols (symbol package)
          (pushnew symbol symbols))
        ;;Internal and external but no inherited
        (do-symbols (symbol package)
          (when (equalp (symbol-package symbol) package)
            (pushnew symbol symbols))))
    symbols))

(defgeneric class-symbols (class &key exclude-super-classes-p
                           only-external-symbols-p)
  (:documentation "Compiles a list of symbols that represents the class name, slot names,
readers, writers for a class and its super classes before
standard-object.

exclude-super-classes-p excludes super classes when set to t.

only-external-symbols-p will only include external symbols for the
class package. This is the default.
"))

(defmethod class-symbols ((class symbol) &key exclude-super-classes-p
                          (only-external-symbols-p t))
  (class-symbols
   (find-class class)
   :exclude-super-classes-p exclude-super-classes-p
   :only-external-symbols-p only-external-symbols-p))

(defmethod class-symbols ((class class) &key exclude-super-classes-p
                          (only-external-symbols-p t))

  (unless (equalp 'standard-object (class-name class))
    (let* ((symbols)
           (class (c2mop:ensure-finalized class))
           (super-classes
             (unless exclude-super-classes-p
               (c2mop:class-direct-superclasses class)))
           (external-symbols (package-symbols (symbol-package (class-name class)))))

      (labels ((push-symbol (symbol)
                 (if only-external-symbols-p
                     (when (find symbol external-symbols)
                       (pushnew symbol symbols))
                     (pushnew symbol symbols))))
        (when super-classes
          (dolist (super super-classes)
            (setf symbols (append symbols
                                  (class-symbols
                                   (class-name super)
                                   :exclude-super-classes-p exclude-super-classes-p
                                   :only-external-symbols-p only-external-symbols-p)))))

        (push-symbol (class-name class))

        (dolist (slot (c2mop:class-slots class))

          (push-symbol (c2mop:slot-definition-name slot)))

        (dolist (slot (c2mop:class-direct-slots class))
          (dolist (reader (c2mop:slot-definition-readers slot))
            (push-symbol reader))
          (dolist (writer (c2mop:slot-definition-writers slot))
            (push-symbol (second writer))))
        symbols))))

(defun merge-allowed-package-symbols (&rest lists)
  "Merges lists of package symbols into one list."
  (let ((allowed-stuff (make-hash-table :test 'equalp))
        (allowed-symbols))

    (dolist (list lists)
      (dolist (package-symbols list)
        (let ((allowed-package (first package-symbols)))
          (dolist (symbol (getf (cdr package-symbols) :allow))
            (pushnew symbol (getf (gethash allowed-package allowed-stuff) :allow)))
          (dolist (symbol (getf (cdr package-symbols) :disallow))
            (pushnew symbol (getf (gethash allowed-package allowed-stuff)
                                  :disallow))))))

    (maphash (lambda (key allowed)
               (setf allowed-symbols
                     (append allowed-symbols
                             (list
                              (list (find-package key)
                                    :allow (getf allowed :allow)
                                    :disallow (getf allowed :disallow))))))
             allowed-stuff)
    allowed-symbols))

(defun devine-allowed-symbols (packages &key only-external-symbols-p)
  "Goes through the list of packages supplied and compiles a list of
allowed symbols."
  (let ((allowed-stuff (make-hash-table :test 'equalp))
        (allowed-symbols))

    (dolist (package packages)
      (dolist (symbol (package-symbols package
                                       :only-external-symbols-p
                                       only-external-symbols-p))
        (pushnew symbol (gethash package allowed-stuff))))

    (maphash (lambda (key allowed)
               (setf allowed-symbols
                     (append allowed-symbols
                             (list
                              (list (find-package key)
                                    :allow allowed)))))
             allowed-stuff)
    allowed-symbols))

(defun strip-stack (stack)
  (let ((new-stack))
    (dolist (item stack)
      (cond ((and (listp (dissect:call item))
                  (equalp (second (dissect:call item)) 'sandbox:eval))
             (return-from strip-stack (reverse new-stack)))
            ((and (listp (dissect:call item))
                  (equalp (second (dissect:call item)) 'run-script)
                  (equalp (third (dissect:call item)) :around))
             (push item new-stack)
             (return-from strip-stack (reverse new-stack)))
            (t
             (push item new-stack))))

    (reverse new-stack)))

(defvar *run-debugging* nil
  "By default errors are surpressed in a script-run and passed back in the
second, third and fourth values of a script-run. This is because it is assumed
that the final user of the script functionality wont have access to
the lisp image directly, some user GUI will inform the user of the
error etc. If you want errors to be raised immediately set
*run-debugging* to T.")

(defvar *debug-load-scripts* nil
  "We gather all the errors for preload scripts during load-scripts. If
you want to raise an error if there are any load issues just after
load-scripts but before the actual script-run code is evaluated then
set this to t.

If you have set *run-debugging* it will override *debug-load-scripts*
behaviour.")

(defparameter *compile-scripts* nil
  "Compiles scripts before running them.")

(defparameter *environments* (make-hash-table :test 'equalp)
  "A hash table containing all the environments added.")

(defparameter *preload-script-errors* nil
  "Perload scripts are loaded during a script-run, but errors are silent
by defualt. Which means we need some other mechanism to report preload
errors. We gather all the preload errors in this variable so that we
can report issues to the user later. Its something like a list of
compile errors.")

(defparameter *muffle-warnings* t
  "Warnings are muffled by default. Set to nil if you want to raise warnings.

Even when warnings are muffled the run will still return them in the message.")

(defconstant script-failure :script-failure
  "The return value of scripts are set to :script-failure if they
failed. This is done because message might still have warnings in them so you cannot rely on just checking for messages on a run.")

(defmacro enable-script-reader-macros (&optional (char #\{) (end-char #\}))
  "Sets in the *READTABLE* the reader macros for reading script code.

When passing a sexp instead of a string to run-script you need to be
carefull because the lisp reader is going to intern any symbols in the
script in the current package. It will also try to find symbols
etc. This means you might get symbol not allowed/found errors or
package does not exist errors.

Wrapping the sexp in #{...} will convert the script to a string which
is then read again during the run with the correct context (packages
etc) setup."

  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (set-dispatch-macro-character
      #\#
      ,char
      #'(lambda (stream subchar arg)
          (declare (ignore subchar arg))
          (with-output-to-string (s)
            (loop for i from 0
                  for c = (read-char stream nil)
                  while (and c (not (char= c ,end-char)))
                  do (write-char c s)))))))

