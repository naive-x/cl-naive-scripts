(in-package :common-lisp-user)

(defpackage :cl-naive-scripts
  (:use :cl)
  (:export
   ;;utilities
   :package-symbols
   :class-symbols
   :devine-allowed-symbols

   ;;scripting
   :*environments*
   :*environment*
   :*script*

   :script
   :name
   :script-package
   :script-use-packages
   :depends-on-packages
   :export-symbols
   :allowed-package-symbols
   :load-order
   :cache
   :load-script-p
   :temp-package-p

   :get-cache

   :environment
   :scripts
   :load-once-p
   :scripts-loaded-p
   :initialized-p

   :get-environment
   :add-environment
   :teardown-environment
   :remove-environment

   :get-script
   :add-script
   :remove-script

   :*debug-load-scripts*
   :load-scripts
   :initialize-environment
   :initialize-run
   :teardown-run

   :*run-debugging*
   :*compile-scripts*
   :*muffle-warnings*
   :script-failure
   :run-script
   :sexp-to-string
   :enable-script-reader-macros))
