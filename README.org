* cl-naive-scripts

A wrapper around sandbox CL to facilitate the use of sandbox as a
scripting engine.

The basic idea is that you have an environment and you can add scripts
to that environment. The environment is something like a system in
lisp. An environment should have a specific purpose for which it is
created and the scripts are there to achieve the purpose.

You can have multiple environments at the same time.

** Documentation

[[docs/docs.org][Documentation]] can be found in the docs folder in the repository.

** Example

This example creates an environment and a script that defines xxx and
yyy functions that will be pre-loaded during environment
initialisation. The package test-environment.exes will be created for
the script and will still be around after the run.

xxx is exported using the script attribute export-symbols where as yyy
will be exported using export-symbol, to show the two ways you can
export a symbol for a script. If you don't export a symbol explicitly
other scripts wont be able to use it.

The functions are then called from a second script that is of a
transient nature. It will not have a lasting package.

Note that script code was wrapped in #{...} reader macro to prevent
lisp from trying to properly read the code. The code is stored in a
string and will be read again in the correct context after the script
run initialisation.


#+BEGIN_SRC lisp
;; Allows the use of {} to wrap script code.
(cl-naive-scripts:enable-script-reader-macros)

(let* ((environment (make-instance 'environment
                                   :name "test-environment"
                                   :load-once-p t))
       (script-to-run
         (add-script
          environment
          (make-instance
           'script
           :name "tmp"
           :transient-package-p t
           :code #{
           (list (test-environment.exes::xxx)
                 (test-environment.exes::yyy))
           }))))

  (add-script
   environment
   (make-instance 'script
                  :preload-script-p t
                  :preload-order 1
                  :name "exes"
                  :export-symbols '(:xxx)
                  :code #{
                  (defun xxx ()
                    "xxx")
                  ;; Can export symbols this way aswell
                  (export-symbol 'yyy)
                  (defun yyy ()
                    "yyy")
                  }))

  ;; run-script returns 3 values
  ;; - the result of the evaluated code
  ;; - any error message
  ;; - a condition-environment that has more info about the error
  ;; The latter two be nil if there were no errors.

  (multiple-value-bind (result message condition-environment)
      (run-script environment script-to-run)

    (prog1
        (list :result result
              :error-message message
              :condition-env condition-environment
              :exes-package (find-package "TEST-ENVIRONMENT.EXES")
              ;; The package wont be found
              :tmp-package (find-package "TEST-ENVIRONMENT.TMP"))
      (teardown-environment environment))))	  
#+END_SRC

** Dependencies

sandbox

** Supported CL Implementations

Should support all compliant implementations, no implementation specific code was used.

** Tests

To load and run the tests, clone the project and then:

#+BEGIN_SRC lisp
  (ql:quickload :cl-naive-scripts.tests)

  (cl-naive-tests:report (cl-naive-tests:run))
#+END_SRC
